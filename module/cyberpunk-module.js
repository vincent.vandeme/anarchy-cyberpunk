import { CharacterHumanite } from './character-humanite.js';
import { MODULE_NAME, STYLE_PATH } from './constants.js';
import { CYBERPUNK_SKILLS } from './skills.js';

const CYBERPUNK_CHECKBARS_HACK = {
  plot: {
    iconChecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/danger-point.webp`, 'checkbar-img'),
    iconUnchecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/danger-point-off.webp`, 'checkbar-img')
  },
  anarchy: {
    iconChecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point.webp`, 'checkbar-img'),
    iconUnchecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point-off.webp`, 'checkbar-img')
  },
  sceneAnarchy: {
    iconChecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point-scene.webp`, 'checkbar-img'),
    iconUnchecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point-off.webp`, 'checkbar-img')
  }
}

const CYBERPUNK_ANARCHY_HACK = {
  id: MODULE_NAME,
  name: 'Cyberpunk Anarchy hack',
  hack: {
    checkbars: () => CYBERPUNK_CHECKBARS_HACK
  }
}

export class CyberpunkModule {
  static start() {
    const cyberpunkModule = new CyberpunkModule();
    Hooks.once('init', async () => await cyberpunkModule.onInit());
  }

  async onInit() {
    game.modules.cyberpunkModule = this;
    Hooks.on(ANARCHY_HOOKS.ANARCHY_HACK, register => register(CYBERPUNK_ANARCHY_HACK));
    Hooks.on(ANARCHY_HOOKS.PROVIDE_SKILL_SET, provide => {
      provide('cyberpunk-anarchy', 'Cyberpunk Anarchy Hack - FR', CYBERPUNK_SKILLS.map(it => {
        it.labelkey = `CYBERPUNK.skill.${it.code}`;
        return it;
      }));
    });
    Hooks.on(ANARCHY_HOOKS.PROVIDE_BASE_ESSENCE, provide => provide(CYBERPUNK_ANARCHY_HACK, actor => CharacterHumanite.getHumanite(actor)));
    Hooks.on(ANARCHY_HOOKS.PROVIDE_MALUS_ESSENCE, provide => provide(CYBERPUNK_ANARCHY_HACK, (actor, essence) => CharacterHumanite.getMalus(actor, essence)));

  }
}

