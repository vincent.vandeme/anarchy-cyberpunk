
export class CharacterHumanite {
  static getHumanite(actor) {
    return actor.system.attributes.willpower.value + actor.system.attributes.charisma.value
  }

  static getMalus(actor, courante) {
    const max = CharacterHumanite.getHumanite(actor);
    const perte = max - courante;
    return Math.min(0, -Math.ceil(perte / 2));
  }
}
